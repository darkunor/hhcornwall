Bien, solo una nota rápida, esta es una versión muy temprana del libro y fue prohibida más tarde. Hemos hecho nuestro mejor esfuerzo para convertirlo a ASCII. Nos ha tomado algo de tiempo armarlo debido al cambio de formato, así que espero que sea apreciado. Hemos mantenido la numeración de página original para que el índice sea correcto.

Imágenes electrónicas complementarias - Gizmo

Century Communications

El manual del hacker

Copyright (c) Hugo Cornwall

Todos los derechos reservados

Publicado por primera vez en Gran Bretaña en 1985 por Century Communications Ltd Portland House, 12-13 Greek Street, Londres W1V 5LE.

Reimpreso en 1985 (cuatro veces)

ISBN 0 7126 0650 5

Impreso y encuadernado en Gran Bretaña por Billing & Sons Limited, Worcester.

CONTENIDOS

Introducción

1 Primeros fundamentos
2 Comunicaciones computadora a computadora
3 Equipamiento de hacker
4 Objetivos: Qué encontrar en los mainframes
5 Inteligencia de hacker
6 Técnicas de hacker
7 Redes
8 Sistemas de visualizacion de datos
9 Datos de ...
10 Hacking: El futuro

Apéndices

I Resolución de problemas
II Glosario
III CCITT y standars relacionados
IV Alfabetos de computadora estandares
V Módems
VI Espectro de radio
VII Diagrama de flujo/flujograma de buscador de puertos

Introducción

La palabra 'hacker' es usada de dos maneras diferentes pero asociadas: para algunos, un hacker es simplemente un entusiasta de las computadoras de cualquier tipo, a quien le encanta trabajar con las fieras por interes propio, en lugar de operarlas para enriquecer a una empresa o proyecto de investigación -- o para jugar juegos.

Este libro utiliza la palabra en un sentido más restringido: el hacking es un deporte recreativo y educativo. Consiste en intentar ingresar sin autorización a las computadoras y explorar lo que hay allí. Los objetivos y propósitos del deporte han sido ampliamente mal interpretados; la mayoría de los hackers no están interesados en cometer fraudes masivos, modificar sus cuentas bancarias personales, en los registros de impuestos y empleados, o en inducir a un superpotencia mundial a iniciar el armagedón sin darse cuenta bajo la errónea creencia de que otra superpotencia está a punto de atacarla. Todos los hackers con los que me he topado han sido bastante claros acerca de dónde reside la diversión: es en desarrollar la comprensión de un sistema y, finalmente, adquirir las habilidades y producir las herramientas para vencerlo. En la gran mayoría de los casos, el proceso de "ingreso" es mucho más satisfactorio de lo que se descubre en los archivos informáticos protegidos.

En este sentido, el hacker es el descendiente directo de los phreaks telefónicos de hace quince años. El phreaking telefónico se volvió interesantes a medida que se introdujo la marcación a larga distancia entre suscriptores intra-nacionales e internacionales, pero cuando el phreak ubicado en Londres finalmente halló la manera de abrirse camino a Hawái, generalmente no tenía a nadie con quien hablar excepto el servicio meteorológico local o la oficina de American Express, para confirmar que el objetivo deseado había sido alcanzado. Una de las primeras hackers de la generación actual, Susan Headley, solo tenía 17 años cuando comenzó sus hazañas en California en 1977, eligió como objetivo a la compañía telefónica local y, con la información extraída de sus hacks, corrió por toda la red telefónica. Ella se "retiró" cuatro años después, cuando mis amigos comenzaron a desarrollar esquemas para cerrar parte del sistema telefónico.

También hay una fuerte afinidad con los crunchers (o rompedores) de protecciones contra la copia de software. La mayoría del software comercial para micro(procesadores) se vende en una forma para evitar la copia casual obvia, por ejemplo cargando un casete, cartucho o disco en la memoria y luego ejecutando un 'guardar' en un casete o disco en virgen. Los dispositivos de protección anticopia varían mucho en su metodología y sofisticación y hay quienes, sin ningún motivo comercial, no disfrutan más que derrotándolos. Todos los aficionados a las computadoras han encontrado al menos un cruncher (rompedor) con una gran cantidad de programas comerciales, a todos los cuales se les ha quitado la protección -- y tal vez el título principal se ha alterado sutilmente para mostrar las habilidades técnicas del cruncher (rompedor) -- pero que en realidad nunca se utilizan en absoluto.

Tal vez debería decirte lo que razonablemente puedes esperar de este manual. El hacking es una actividad como pocas: es semilegal, rara vez se fomenta, y en toda su extensión es tan vasta que ningún individuo o grupo, a excepción de una organización como la GCHQ o la NSA, podría esperar aprovechar una fracción de las posibilidades. Así que este no es uno de esos libros con títulos como Programación de Juegos con el (procesador) 6502 donde, si el libro es suficientemente bueno y si tu eres suficientemente bueno, emergerás con algún dominio en la materia. El objetivo de este libro es simplemente darte una idea de la metodología, ayudarte a desarrollar las actitudes y habilidades idóneas, proporcionarte los fundamentos esenciales y algún material de referencia -- y orientarte en las direcciones correctas para obtener más conocimiento. Hasta cierto punto, cada capítulo puede ser leído por sí mismo; he compilado extensos apéndices, que contienen material que será de utilidad mucho después de que el cuerpo principal del texto haya sido absorbido.

Una de las características de las anécdotas del hacking, al igual que aquellas relacionadas con las proezas del espionaje, es que casi nadie estrechamente implicado tiene mucho que ver con la verdad; las víctimas quieren describir el daño como mínimo, y los perpetradores les gusta pintarse a sí mismos como héroes mientras que cuidadosamente disfrazan las fuentes y los métodos utilizados. Además, los periodistas que cubren tales historias no siempre son lo suficientemente competentes para escribir con precisión, o incluso para saber cuando están siendo engañados. (Una nota para los periodistas: cualquier hacker que se ofrezca a entrar en un sistema por encargo te está estafando -- lo máximo que puedes esperar es una representacion para tu beneficio de lo que un hacker ha hecho anteriormente de manera exitosa. Llegar a la "portada" de un servicio o red no tiene por qué implicar que todo dentro de ese servicio puede haber sido accedido. Ser capaz de recuperar información confidencial, tal vez evaluaciones de crédito, no significa que el hacker también sea capaz de alterar esos datos. Recuerda la primera regla de un buen reportaje: ser escéptico.) En la medida de lo posible, he intentado verificar cada historia que aparece en estas páginas, pero los hackers trabajan en grupos aislados y mis fuentes sobre algunos de los hacks más importantes de los últimos años son más lejanos de lo que me hubiera gustado. En estos casos, mis relatos se refieren a acontecimientos y métodos que, en todos los casos circunstancias, creo que son ciertas. Son bienvenidos los comentarios de corrección.
























